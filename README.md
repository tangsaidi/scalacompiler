# LacsCompiler

This is a simple compiler for Lacs ("Scala" backwards and short of an 'a'). It compiles Lacs language into MIPS instruction set.

The compiler supports various functionalities, including calls, loops and nested procedures.
