import cs241e.assignments.ProgramRepresentation._
import cs241e.assignments.CodeBuilders._
import cs241e.assignments.{A1, Debugger, ProgramRepresentation, Reg}
import cs241e.assignments.Transformations._
import cs241e.assignments.Assembler._
import cs241e.mips._
import org.scalatest.FunSuite
import org.scalatest.prop.Configuration.Workers


class A6Tests extends FunSuite {
  test("scala") {

    def increaseBy2(increment: Int): (Int)=>Int = {
      def procedure(x: Int) = { x + increment }
      procedure
    }
    def main(a: Int, b: Int) = (increaseBy2(a))(b)

    assert(main(3,5)==8)
  }

  test("nested"){
    def v(variable: Variable): Code = read(Reg.result, variable)
    val a = new Variable("a")
    val b = new Variable("b")
    val argc = new Variable("argc")
    val argv = new Variable("argv")
    val f = new Procedure("f", Seq(a, b))
    val g = new Procedure("g", Seq(), Some(f))
    val main = new Procedure("main", Seq(argc, argv))
    main.code = Call(f, Seq(v(argc), v(argv)))
    f.code = Call(g, Seq())
    g.code = binOp(v(a), plus, v(b))
    val machineCode = compilerA6(Seq(main, f, g))
    val endState = A1.loadAndRun(machineCode.words,
      Word(encodeSigned(3)), Word(encodeSigned(5)))
    println("THE VALUE IS")
    println(decodeSigned(endState.reg(3)))
  }

  test("lacs") {
    def v(variable: Variable): Code = read(Reg.result, variable)

    val increment = new Variable("increment")
    val increaseBy = new Procedure("increaseBy", Seq(increment))

    val x = new Variable("x")
    val procedure = new Procedure("procedure", Seq(x), Some(increaseBy))

    procedure.code = binOp(v(x), plus, v(increment))
    increaseBy.code = Closure(procedure)

    val a = new Variable("a")
    val b = new Variable("b")
    val parameter = new Variable("parameter")

    val main = new Procedure("main", Seq(a, b))
    main.code =
      CallClosure(
        call(increaseBy, v(a)), Seq(v(b)), Seq(parameter)
      )


    val machineCode = compilerA6(Seq(main, increaseBy, procedure))

    val initState = A1.setMem(machineCode.words)
    val endState = A1.loadAndRun(machineCode.words,
      Word(encodeSigned(100)), Word(encodeSigned(56)))
    println(decodeSigned(endState.reg(3)))

    //    val finalState = Debugger.debug(initState,machineCode.debugTable)
    //    println(finalState


//    val endState = A1.loadAndRun(machineCode.words,
//      Word(encodeSigned(3)), Word(encodeSigned(5)))
//    println(decodeSigned(endState.reg(3)))
  }

}
