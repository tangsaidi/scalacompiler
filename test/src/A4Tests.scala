import cs241e.assignments.ProgramRepresentation._
import cs241e.assignments.Assembler._
import cs241e.assignments._
import cs241e.assignments.Transformations._
import cs241e.assignments.CodeBuilders._

import cs241e.mips.{CPU, Word}
import org.scalatest.FunSuite

class A4Tests extends FunSuite {
//  test("IF STMT"){
//    println("IF STMT TEST")
//    val code = block(
//      LIS(Reg(1)),
//      Word(encodeSigned(42)),
//      LIS(Reg(2)),
//      Word(encodeSigned(43)),
//      ifStmt(ADD(Reg.result, Reg(1)), eqCmp, ADD(Reg.result, Reg(2)),
//        block(LIS(Reg(10)), WORD(1)),
//        block(LIS(Reg(10)), WORD(3))),
//      LIS(Reg(11)), WORD(3)
//    )
//
//    val compiled = compilerA4(code)
//    val finalState = A4.loadAndRun(compiled)
//    println(finalState)
//
//  }
//
//  test("last element") {
//    println("LAST ELEMENT TEST")
//    println(A4.loadAndRunArray(A4.lastElement, Seq(WORD(1), WORD(1234), WORD(4))))
//  }
//
//
//  test("maximum test"){
//    println("MAXIMUM TEST")
//    println(A4.loadAndRunArray(A4.arrayMaximum, Seq(WORD(1), WORD(8), WORD(4))))
//  }
//
  test("print output letters test "){
    println("OUTPUT LETTERS TEST")
    println(A4.loadAndRunArray(A4.outputLetters, Seq(WORD(8), WORD(5), WORD(12),WORD(12), WORD(15),
      WORD(0), WORD(23), WORD(15), WORD(18), WORD(12), WORD(4))))
  }

  test("print integer test"){
    println("PRINT INTEGER TEST")

    val code = block(
      LIS(Reg(1)),
      Word(encodeSigned(2146959360)),
      A4.printIntegerCode
    )

    val compiled = compilerA4(code)
    val finalState = A4.loadAndRun(compiled)
    println(finalState)
  }

}
