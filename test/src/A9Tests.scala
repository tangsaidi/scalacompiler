import cs241e.assignments.Assembler._
import cs241e.assignments.{A4, Lacs}
import cs241e.mips.Word
import org.scalatest.FunSuite

import cs241e.assignments.Assembler.{decodeSigned, encodeSigned}
  import cs241e.assignments.{A4, Lacs}
    import cs241e.mips.Word
    import org.scalatest.FunSuite
class A9Tests extends FunSuite {
  test("test") {
    def good(prog: String) =
      Lacs.scanAndParseAndType(prog)
    def bad(prog: String) = assertThrows[RuntimeException](
      Lacs.scanAndParseAndType(prog)

    )
    good("def main(a: Int, b: Int): Int = { a+b }")
    good("def main(a: Int, b: Int): Int = { a*b }")
    good("def main(a: Int, b: Int): Int = { a%b }")
    good("def main(a: Int, b: Int): Int = { a/b }")
    good("def main(a: Int, main: Int): Int = {0}")
    good("def main(a: Int, b: Int): Int = {0}")
    good("def main(a: Int, b: Int): Int = {var main: Int; 0}")
    good("def main(a: Int, b: Int): Int = { def main(): Int = { 0 } 0 }")
    good("def main(a: Int, b: Int): Int = { main(1,2) }")
    good("def main(a: Int, b: Int): Int = { def c(): Int = { def b(): Int = { 0 } 0 } 0 }")
    good("def main(a: Int, b: Int): Int = { def c(): Int = { var a: Int; 0 } 0 }")
    good("def main(a: Int, b: Int): Int = { var c: Int; c = c }")
    good("""
           def main(a: Int, b: Int): Int = {
            foo()
           }
           def foo(): Int = {
            main(0, 0)
            }
           """.stripMargin.replaceAll("\r", "")
    )

    good("""
    def main(a: Int, b: Int): Int = {
      def foo(): Int = {
        main(0, 0)
      }
      foo()
    }

      """.stripMargin.replaceAll("\r", ""))

    good("""def main(a: Int, b: Int): Int = {
      def bar(): Int = {
        def foo(): Int = {
          main(0, 0)
        }
        foo()
      }
      bar()
    }""".stripMargin.replaceAll("\r", ""))


    good("""
           def main(a: Int, b: Int): Int = { var c: Int; c = c; 0 }
           """.stripMargin.replaceAll("\r", ""))

    good("""
        def main(a: Int, b: Int): Int = {
          foo()
        }
        def foo(): Int = { 0 }""".stripMargin.replaceAll("\r", ""))

    good(
      """
        def main(a: Int, b: Int): Int = {
         a + b
        }

        def notmain(a: Int, b: (Int) => (Int) => Int): Int = {
        a + b(0)(1)
        }
      """.stripMargin.replaceAll("\r", "")
    )

    good(
      """
        def main(x: Int, y: Int): Int = {
          var i: Int;
          var ret: Int;

          def loop(): Int= {
            if (i < x) {
              i = i + 1;
              ret = ret * i;
              loop()
            } else {
              0
            }
          }
          ret = 1;
          i = 1;
          loop();
          ret
        }
      """.stripMargin.replaceAll("\r", "")
    )
    println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")



    /** my test cases **/
    println("My test case 1")
    good("""
           def main(a: Int, b: Int): Int = {
            foo();
            5
           }

           def foo(): Int = {
            var func: (Int) => Int;
            def bar(): (Int) => Int = {
              def baz(a: Int): Int = {
                a * 2
              }
              baz
            }
            func = bar();
            func(3)
            }
           """.stripMargin.replaceAll("\r", ""))

    println("*****************************")
    println("my test case 2")
    good("""
           def main(a: Int, b: Int): Int = {
            var ret: Int;
            ret = 5 + 10;
            ret * 42
           }
           """.stripMargin.replaceAll("\r", ""))

    println("*****************************")
    println("my test case 3")
    bad("""
           def main(a: Int, b: Int): Int = {
            foo();
            5
           }

           def foo(): Int = {
            var func: (Int) => Int;
            def bar(): (Int) => Int = {
              def baz(a: Int): Int = {
                a * 2
              }
              baz
            }
            func = bar();
            func() // missing parameter
            }
           """.stripMargin.replaceAll("\r", ""))

    println("*****************************")
    println("my test case 3")
    bad( """
           def main(a: Int, b: Int): Int = {
            def foo(c : Int, d : (Int) => Int): Int= {
              d(c)
            }
            def bar(c: Int): Int = {
              c + 1000
             }
             foo(20, 30)
           }
           """.stripMargin.replaceAll("\r", ""))

    println("*****************************")
    println("my test case 4")
    good("""
           def main(a: Int, b: Int): Int = {
           var temp: Int;
           var tempFunc: (Int) => Int;
            def foo(c : Int, d : (Int) => Int): Int= {
              d(c)
            }
            def bar(c: Int): Int = {
              c + 1000
             }
             foo(20, bar);
             tempFunc = bar;
             foo(20, tempFunc)
           }
           """.stripMargin.replaceAll("\r", ""))

  }

  test("A9SingleTest") {
    val prog = """
           def main(a: Int, b: Int): Int = {
           var temp: Int;
           var tempFunc: (Int) => Int;
            def foo(c : Int, d : (Int) => Int): Int= {
              d(c)
            }
            def bar(c: Int): Int = {
              c + 1000
             }
             foo(20, bar);
             tempFunc = bar;
             foo(20, tempFunc)
           }
           """.stripMargin.replaceAll("\r", "")
    Lacs.scanAndParseAndType(prog)


  }
  test("A10test") {
    def run(prog: String, a: Int = 0, b: Int = 0) = {
      val machineCode = Lacs.compile(prog)
      val finalState =
        A4.loadAndRun(machineCode, Word(encodeSigned(a)), Word(encodeSigned(b)))
      println(decodeSigned(finalState.reg(3)))
    }

    run("def main(a: Int, b: Int): Int = { a + b }", 5, 6)
    run("""
        def main(a: Int, b: Int): Int = {
           closure()(a, b)
        }

        def closure(): (Int, Int) => Int = {
           def fun(a: Int, b: Int): Int = {
              a + 1
           }
           fun
        }
      """.stripMargin.replaceAll("\r", ""), 10, 101)


  }
}