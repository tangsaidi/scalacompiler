import cs241e.assignments.ProgramRepresentation._
import cs241e.assignments.Assembler._
import cs241e.assignments._
import cs241e.assignments.Transformations._

import cs241e.mips.{CPU, Word}
import org.scalatest.FunSuite

class A3Tests extends FunSuite {
  test("A3 Compiler"){
    val variable = new Variable("variable")

    val code = block(
      LIS(Reg.scratch),
      Word(encodeSigned(42)),
      write(variable, Reg.scratch),
      read(Reg.result, variable)
    )

    val compiled = compilerA3(code, Seq(variable))
    val initState = A1.setMem(compiled.words)
    val finalState = CPU.run(initState)
    println(finalState)

  }

}
