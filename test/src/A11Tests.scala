import cs241e.assignments.{A1, A4, MemoryManagement, Reg}
import cs241e.assignments.Assembler._
import cs241e.assignments.MemoryManagement._
import cs241e.assignments.ProgramRepresentation._
import cs241e.assignments.CodeBuilders._
import cs241e.assignments.Transformations._
import cs241e.mips.{State, Word}
import org.scalatest.FunSuite
import org.scalatest.prop.Configuration.Workers

class A11Tests extends FunSuite {
  test("gcTest") {
    MemoryManagement.heap = GarbageCollector

    val a = new Variable("a")
    val b = new Variable("b")
    val p = new Variable("p", isPointer = true)
    val q = new Variable("q", isPointer = true)
    val main = new Procedure("main", Seq(a, b))

    val f = new Variable("f", isPointer = true)
    val chunk = new Chunk(Seq(f))

    def allocateChunk(p: Variable, chunk: Chunk): Code = block(
      MemoryManagement.heap.allocate(chunk),
      write(p, Reg.allocated)
    )

    def writeField(base: Code, field: Variable, value: Code): Code =
      binOp(base, chunk.store(Reg.scratch, f, Reg.result), value)

    def v(v: Variable) = read(Reg.result, v)

    main.code = Scope(Seq(p, q), block(
      allocateChunk(p, chunk),
      allocateChunk(q, chunk),
      writeField(v(p), f, v(q)),
      //      assign(p, v(q)),
      assign(q, v(p)),
      call(GarbageCollector.collectGarbage),
      //      call(GarbageCollector.collectGarbage),
      ADD(Reg.zero, Reg.zero)
    ))

    val machineCode = compilerA6(main +: GarbageCollector.procedures)
    val finalState = A4.loadAndRun(machineCode, debug = true)

    def spaces(w: Word): String =
      w.toString.sliding(8,8).mkString(" ")

    def dumpMem(state: State, address: Word, words: Int = 6): Unit = {
      if(words > 0) {
        println(spaces(address) + ": " + spaces(state.mem(address)))
        dumpMem(state, Word(encodeUnsigned(decodeUnsigned(address)+4)), words-1)
      }
    }
    println("From")
    dumpMem(finalState, GarbageCollector.heapStart)
    println("To")
    dumpMem(finalState, GarbageCollector.heapMiddle)
    println("reachable: "+ decodeSigned(finalState.reg(3)))
    println("heapPtr: "+ spaces(finalState.reg(28)))
  }
}