import cs241e.assignments.{A1, Assembler}
import cs241e.mips.Word
import org.scalatest.FunSuite

/* This is an example of a class for running and testing your code. Add other testing methods here
 * and/or create additional classes like this one.
 *
 * Run the tests by right-clicking on the test code and selecting Run '(test name)' from the menu.
 *
 * You can also run the tests by right-clicking on a file or directory containing tests in the Project navigator
 * on the left.
 */

class A1Tests extends FunSuite {
  test("decodeUnsigned") {

    println("You can print output from your tests.")


    assert(1 + 1 == 2, "1 + 1 did not equal 2.")

    // The following will fail until you implement decodeUnsigned as part of Assignment 1.
    println(Assembler.decodeUnsigned(Seq(false, true, true, false)))
    println(Assembler.decodeUnsigned(Seq(true,true)))

    println(Assembler.decodeSigned(Seq(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false)))

    println(Assembler.encodeUnsigned(1024))
    println(Assembler.encodeUnsigned(4))

    println(Assembler.encodeSigned(1024))
    println(Assembler.encodeSigned(-4))


    // Check whether the encoding and decoding are equivalent
    assert(0 == Assembler.decodeUnsigned(Assembler.encodeUnsigned(0)), "Encode and decoding give different results for unsigned 0")
    assert(1 == Assembler.decodeUnsigned(Assembler.encodeUnsigned(1)), "Encode and decoding give different results for unsigned 1")
    assert(3 == Assembler.decodeUnsigned(Assembler.encodeUnsigned(3)), "Encode and decoding give different results for unsigned 3")
    assert(1111 == Assembler.decodeUnsigned(Assembler.encodeUnsigned(1111)), "Encode and decoding give different results for unsigned 1111")
    assert(4294967295L == Assembler.decodeUnsigned(Assembler.encodeUnsigned(4294967295L)), "Encode and decoding give different results for signed long")
    println("Passed all unsigned integer tests")

    assert(0 == Assembler.decodeSigned(Assembler.encodeSigned(0)), "Encode and decoding give different results for signed 0")
    assert(-1 == Assembler.decodeSigned(Assembler.encodeSigned(-1)), "Encode and decoding give different results for signed -1")
    assert(1 == Assembler.decodeSigned(Assembler.encodeSigned(1)), "Encode and decoding give different results for signed 1")
    assert(2147483647 == Assembler.decodeSigned(Assembler.encodeSigned(2147483647)), "Encode and decoding give different results for signed Long")
    assert(-2147483648 == Assembler.decodeSigned(Assembler.encodeSigned(-2147483648)), "Encode and decoding give different results for signed Long")
    assert(2 == Assembler.decodeSigned(Assembler.encodeSigned(2)), "Encode and decoding give different results for signed 2")
    assert(123 == Assembler.decodeSigned(Assembler.encodeSigned(123)), "Encode and decoding give different results for signed 123")
    assert(-123 == Assembler.decodeSigned(Assembler.encodeSigned(-123)), "Encode and decoding give different results for signed -123")
    assert(-999 == Assembler.decodeSigned(Assembler.encodeSigned(-999)), "Encode and decoding give different results for signed -999")
    println("Passed all signed integer tests")


    try{
      Assembler.encodeSigned(2147483648L)
    } catch {
      case e: IllegalArgumentException => println("Caught signed number that are too large exception")
    }
    try{
      Assembler.encodeSigned(4294967295L)
    } catch {
      case e: IllegalArgumentException => println("Caught unsigned number that are too large exception")
    }

    println("Check MIPS code")
    val state31 = A1.loadAndRun(A1.returnTo31)
    val state134 = A1.loadAndRun(A1.add134, Word("00000000000000000000000000000011"),  Word("00000000000000000000000000000001"))

    val stateMax = A1.loadAndRun(A1.maximum, Word("10000000000000000000000000000011"),  Word("10000000000000000000000001000001"))
    println(stateMax.reg(3))

    val stateAdd1 = A1.loadAndRun(A1.addOne, Word("00000000000000000000000000000011"))
    println(stateAdd1.reg(3))

    val stateFollowingAddress = A1.loadAndRun(A1.followingAddress, Word("00000000000010110000000000000000"))
    println(stateFollowingAddress.reg(3))
    println("Done")
  }
}
