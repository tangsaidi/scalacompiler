import cs241e.assignments.Assembler._
import cs241e.assignments.CodeBuilders._
import cs241e.assignments.ProgramRepresentation._
import cs241e.assignments.Transformations._
import cs241e.assignments.{A1, A4, A5, Reg}
import cs241e.mips.Word
import org.scalatest.FunSuite


class A5TestsPrintArr extends FunSuite {
  test("fact") {
    def c(i: Int) = block(
      LIS(Reg.result),
      Word(encodeSigned(i))
    )
    def v(v: Variable) = read(Reg.result, v)

    val a = new Variable("a")
    val b = new Variable("b")
    val main = new Procedure("main", Seq(a, b))
    main.code = call(A5.printArray , v(a), v(b))



    val machineCode = compilerA5(Seq(main, A5.printArray, A5.printProcedure))
    A4.loadAndRunArray(machineCode,
      Seq(WORD(1), WORD(2), WORD(3), WORD(4), WORD(5)), debug = false)


  }
}