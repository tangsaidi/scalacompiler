import cs241e.assignments.{Lacs, Parsing}
import cs241e.scanparse.DFAs._
import cs241e.scanparse.Grammars
import org.scalatest.FunSuite

class A8Tests extends FunSuite {
  test("simpleGrammar") {
    val grammar = Grammars.parseGrammar(
      """
        S expr op expr op expr
        expr ID
        op +
        op *
      """.stripMargin)

    println(grammar.nonTerminals)
    println(grammar.terminals)
    println(grammar.productions)

    val tokens = Seq(Token("ID"), Token("+"), Token("ID"), Token("*"),Token("ID"))

    val tree = Parsing.parseCYK(grammar, tokens.toIndexedSeq).get
    println("result")
    println(tree)

  }
  test("lacsExample") {
    val prog = "def main(a: Int, b: Int): Int = {a + b + 5 * 10}"
    val tokens = Lacs.scan(prog)
    println(tokens)
    val tree2 = Lacs.scanAndParse(prog)
    println(tree2)
  }

  test("lacsExample2") {
    val prog = "def main(a: Int, b: Int): Int = {a + b + 5 * 10}"
    val tokens = Lacs.scan(prog)
    println(tokens)
    val tree = Lacs.scanAndParse(prog)
    println(tree)
  }
}
