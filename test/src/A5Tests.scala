import org.scalatest.FunSuite
import cs241e.assignments.ProgramRepresentation._
import cs241e.assignments.Assembler._
import cs241e.assignments.{A1, A5, A4,  Reg}
import cs241e.mips.Word
import cs241e.assignments.Transformations._
import cs241e.assignments.CodeBuilders._


class A5Tests extends FunSuite {
  test("fact") {
    def fact2(i: Int): Int = if(i<=0) 1 else i*fact2(i-1)
    println(fact2(10))

    def c(i: Int) = block(
      LIS(Reg.result),
      Word(encodeSigned(i))
    )
    def v(v: Variable) = read(Reg.result, v)

    val i = new Variable("i")
    val fact = new Procedure("fact", Seq(i))
    fact.code = ifStmt(
      v(i), leCmp, c(0),
      c(1),
      binOp(v(i), times,
        call(fact, binOp(v(i), minus, c(1)))
      )
    )

    val a = new Variable("a")
    val b = new Variable("b")
    val main = new Procedure("main", Seq(a, b))
    main.code = call(fact, v(a))

    val machineCode = compilerA6(Seq(main, fact))
    val finalState = A1.loadAndRun(machineCode.words,
      Word(encodeSigned(10)))
    println(decodeSigned(finalState.reg(3)))
  }
}