package cs241e.assignments

import ProgramRepresentation._
import cs241e.scanparse.Grammars._

import scala.collection.mutable

/** Implementation of semantic analysis for the Lacs language. */

object Typer {
  /** Representation of a Lacs type, which is either an Int or a function type with parameter types and a return type.
    */
  sealed abstract class Type
  case object IntType extends Type
  case class FunctionType(parameterTypes: Seq[Type], returnType: Type) extends Type

  /** Given a `tree`, finds all descendants of the `tree` whose root node has kind `lhsKind`.
    * Does not search within the found subtrees for any nested occurrences of additional descendants.
    *
    * For example, searching the root of a program tree with `lhsKind = "procedure"` will return the trees all
    * of the top-level procedures, but not any procedures nested within them.
    */
  def collect(tree: Tree, lhsKind: String): Seq[Tree] =
    if(tree.lhs.kind == lhsKind) Seq(tree) else tree.children.flatMap((tree: Tree) => collect(tree, lhsKind))

  /** Given a tree that is either a "type" or contains exactly one "type" nested within it, returns
    * an instance of `Type` representing the corresponding type.
    */
  def parseType(tree: Tree): Type = {
    def parseTypes(treeTypes: Tree): Seq[Type] = {
      if (treeTypes.children.size == 1) Seq(parseType(treeTypes.children.head))
      else parseType(treeTypes.children.head) +: parseTypes(treeTypes.children.last)
    }

    val types = collect(tree, "type")
    require(types.size == 1)
    val treeChildren = types.head.children
    if (treeChildren.length == 1) IntType
    else {
      val typesNode = collect(treeChildren(1), "types").head
      FunctionType(parseTypes(typesNode), parseType(treeChildren(4)))}
  }

  /** A variable combined with its declared type. */
  case class TypedVariable(variable: Variable, tpe: Type)

  /** Create a new `Variable` given its `name` and type `tpe`. */
  def makeVariable(name: String, tpe: Type): Variable =
    new Variable(name, isPointer = (tpe != IntType))

  /** A `SymbolTable` maps each name to either a `TypedVariable` or a `ProcedureScope`. */
  type SymbolTable = Map[String, Either[TypedVariable, ProcedureScope]]

  /** Given a tree containing subtrees rooted at "vardef", creates a `TypedVariable` for each such tree. */
  def parseVarDefs(tree: Tree): Seq[TypedVariable] = {
    collect(tree, "vardef").map{ varDef => {
      val varType =  parseType(varDef.children(2))
      val variableName = varDef.children.head.lhs.lexeme
      val tempVar = makeVariable(variableName, varType)
      TypedVariable(tempVar, varType)
    }
    }
  }

  /** Call `sys.error()` if any `String` occurs in `names` multiple times. */
  def checkDuplicates(names: Seq[String]): Unit = {
    val duplicates = names.diff(names.distinct)
    if(duplicates.nonEmpty) sys.error(s"Duplicate identifiers ${duplicates}")
  }

  /** A `ProcedureScope` holds the semantic information about a particular procedure that is needed to type-check
    * the body of the procedure, including information coming from outer procedure(s) within which this
    * procedure may be nested.
    *
    * @param tree the tree defining the procedure (rooted at a "defdef")
    * @param outer the `ProcedureScope` of the outer procedure that immediately contains this one
    */
  class ProcedureScope(tree: Tree, outer: Option[ProcedureScope] = None) {
    assert(tree.production ==
      "defdef DEF ID LPAREN parmsopt RPAREN COLON type BECOMES LBRACE vardefsopt defdefsopt expras RBRACE")
    val Seq(_, id, _, parmsopt, _, _, retTypeTree, _, _, vardefs, defdefs, expras, _) = tree.children

    /** The name of the procedure. */
    val name: String = id.lhs.lexeme

    /** The parameters of the procedure. */
    val parms: Seq[TypedVariable] = parseVarDefs(parmsopt)

    /** The variables declared in the procedure. */
    val variables: Seq[TypedVariable] = parseVarDefs(vardefs)

    /** The declared return type of the procedure. */
    val returnType: Type = parseType(retTypeTree)

    /** The new `Procedure` object that will represent this procedure. */
    val procedure: Procedure = new Procedure(name, parms.map{x => x.variable},
      if (outer.isEmpty) None else Some(outer.get.procedure))

    /** The `ProcedureScope`s of the nested procedures that are immediately nested within this procedure.
      *
      * Note: this `val` will recursively call `new ProcedureScope(...)`.
      */
    val subProcedures: Seq[ProcedureScope] = collect(defdefs, "defdef").map{
      x => new ProcedureScope(x, Some(this))
    }

    /** The names of parameters, variables, and nested procedures that are newly defined within this procedure
      * (as opposed to being inherited from some outer procedure).
      */
    val newNames: Seq[String] = parms.map{x => x.variable.name} ++ variables.map{x => x.variable.name} ++ subProcedures.map{x => x.name}
    println(newNames)


    checkDuplicates(newNames)

    /** The symbol table to be used when type-checking the body of this procedure. It should contain all
      * symbols (parameters, variables, nested procedures) defined in this procedure, as well as those
      * defined in outer procedures within which this one is nested. Symbols defined in this procedure
      * override (shadow) those of outer procedures.
      *
      * The symbol table is first initialized to null, and filled in later, after this `ProcedureScope`
      * has been constructed, by calling the `createSymbolTable` method. This is necessary
      * because computation of the `symbolTable` depends on the `symbolTable`s of any
      * outer procedures within which this procedure is nested. However, their `symbolTable`s contain
      * the `ProcedureScope`s for the procedures nested within them, which are not created until after
      * the enclosing procedure is created.
      */
    var symbolTable: SymbolTable = null

    /** Create a symbol table for the current procedure, and update the `symbolTable` field with it.
      * See the comments for `val symbolTable` for details on what the `symbolTable` should contain.
      *
      * The `outerSymbolTable` parameter contains the symbol table of the enclosing scope (either an
      * outer procedure within which the current procedure is nested, or, if the current procedure
      * is a top-level procedure, a symbol table containing the names of all of the top-level procedures).
      *
      * This method must also recursively call `createSymbolTable` on all of its `subProcedures` to
      * construct their symbol tables as well.
      */
    def createSymbolTable(outerSymbolTable: SymbolTable): Unit = {
      if (outerSymbolTable != null) symbolTable = outerSymbolTable else symbolTable= Map[String, Either[TypedVariable, ProcedureScope]]()
      symbolTable = symbolTable ++ (newNames zip (parms.map{x => Left(x)} ++ variables.map{x => Left(x)} ++ subProcedures.map{x => Right(x)}))
      subProcedures.foreach(proc => proc.createSymbolTable(symbolTable))
    }

    /** Returns a sequence containing `this` `ProcedureScope` and the `ProcedureScope`s for all procedures
      * declared inside of this procedure, including those nested recursively within other nested procedures.
      *
      * Scala hint: learn about the `flatMap` method in the Scala library. If you are not familiar with flatMap,
      * one place you can read about it is here:
      * http://www.artima.com/pins1ed/working-with-lists.html#sec:higher-order-methods
      */
    def descendantScopes: Seq[ProcedureScope] = this +: this.subProcedures.flatMap(proc => proc.descendantScopes)
  }

  /** Checks that the body of a procedure satisfies the type-checking rules in the Lacs language specification.
    * Returns a `Map` that provides a `Type` for each `Tree` that has a `Type` according to the language
    * specification.
    */

  def typeCheck(scope: ProcedureScope): Map[Tree, Type] = {
    /** The map that will be returned containing the `Type` of each `Tree` that has a `Type`. */
    val treeToType = mutable.Map[Tree, Type]()

    /** Calls `runtime exception` if `tpe1` and `tpe2` are not equal. If they are equal, returns them. */
    def mustEqual(tpe1: Type, tpe2: Type): Type =
      if(tpe1 == tpe2) tpe1 else throw new RuntimeException(s"Type mismatch: expected $tpe2, got $tpe1")

    /** For a `tree` rooted at a node that has a `Type`, computes the `Type`, adds it to `treeToType`,
      * and returns it.
      */

    def typeOf(tree: Tree): Type = treeToType.getOrElseUpdate(tree, {
      def typeOfExpr(expr: Tree): Type = treeToType.getOrElseUpdate(expr, {
        def typeOfTerm(term: Tree): Type = treeToType.getOrElseUpdate(term, {
          def typeOfFactor(factor: Tree): Type = treeToType.getOrElseUpdate(factor, {
            def typeOfArgs(arg: Tree): Seq[Type] = {
              if (arg.children.length == 1) Seq(treeToType.getOrElseUpdate(arg, typeOfExpr(arg.children.head)))
              else Seq(treeToType.getOrElseUpdate(arg, typeOfExpr(arg.children.head))) ++ typeOfArgs(arg.children(2))
            }

            /** type of Factor **/
            if (factor.children.size == 1){
              /** simple base case **/

              val kind = factor.children.head.lhs.kind
              val lexeme = factor.children.head.lhs.lexeme
              kind match{
                case "ID" => {
                  if (!scope.symbolTable.isDefinedAt(lexeme)) throw new RuntimeException(s"Symbol '$lexeme' is not defined")
                  scope.symbolTable(lexeme) match {
                  case Left(x) => x.tpe
                  case Right(x) => FunctionType(x.parms.map{
                    x => x.tpe
                  }, x.returnType)
                }}
                case "NUM"=> IntType
              }
            } else if (factor.children.size == 3){
              /** case where factor contains an expression **/
              typeOfExpr(factor.children(1))
            } else {
              val factSelfType: Type = typeOfFactor(factor.children.head)

              val funcTuple = factSelfType match {
                case IntType => throw new RuntimeException("Int does not take parameter")
                case FunctionType(parameterTypes, returnType) => (parameterTypes, returnType)
              }
              val argsopt = factor.children(2)
              if (argsopt.children.isEmpty && funcTuple._1.nonEmpty) throw new RuntimeException("Expected paramters not fulfilled")
              if (argsopt.children.isEmpty) funcTuple._2
              else {
                val arg = argsopt.children.head
                val argSeq = typeOfArgs(arg)
                if (argSeq.length != funcTuple._1.length) throw new RuntimeException("The number of arguments do not equal to number of parameters")
                (funcTuple._1 zip argSeq ).foreach{
                  checkParamArg => mustEqual(checkParamArg._2, checkParamArg._1)
                }
                funcTuple._2
              }
            }
          })

          /** type of Term **/
          if (term.children.size == 1) typeOfFactor(term.children.head)
          else {
            mustEqual(typeOfTerm(term.children.head), IntType)
            mustEqual(typeOfFactor(term.children.last), IntType)
            IntType
          }
        })
        /** type of Expr **/
        if (expr.children.size == 1) typeOfTerm(expr.children.head)
        else if (expr.children.size == 3) {
          mustEqual(typeOfExpr(expr.children.head), IntType)
          mustEqual(typeOfTerm(expr.children.last), IntType)
        } else {
          val test = expr.children(2)
          mustEqual(typeOfExpr(test.children.head), IntType)
          mustEqual(typeOfExpr(test.children.last), IntType)
          val firstExpras = typeOf(expr.children(5))
          val secondExpras = typeOf(expr.children(9))
          mustEqual(firstExpras, secondExpras)
          firstExpras
        }
      })

      def typeOfExpra(expra: Tree): Type = treeToType.getOrElseUpdate(expra, {
        if (expra.children.length == 1) typeOfExpr(expra.children.head)
        else {
          val rightSideType = typeOfExpr(expra.children.last)
          val lexeme = expra.children.head.lhs.lexeme
          if (!scope.symbolTable.isDefinedAt(lexeme)) throw new RuntimeException(s"Symbol '$lexeme' is not defined")
          val leftSideType: Type = scope.symbolTable(expra.children.head.lhs.lexeme) match {
            case Left(x) => x.tpe
            case Right(value) => sys.error("The ID must be a variable")
          }
          mustEqual(rightSideType, leftSideType)
          leftSideType
        }
      })


      /** type of Tree **/

      val expras = if (tree.children.size == 1) null else tree.children(2)

      val result = typeOfExpra(tree.children.head)
      if (expras != null) typeOf(expras) else result
    })

    /* Check that the type of the expression returned from the procedure matches the declared type of the procedure. */
    mustEqual(typeOf(scope.expras), scope.returnType)
    
    Map() ++ treeToType
  }
}
