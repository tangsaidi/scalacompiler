package cs241e.assignments

import cs241e.mips._

import scala.annotation.tailrec

/** An assembler that generates machine language words representing MIPS instructions. */

object Assembler {

  /* ## Assignment 1 */

  /* Complete the implementation of the following methods by replacing the `???`. */

  /** Converts Boolean to Integer and integer to boolean
    **/
  def boolToInt(bool: Boolean): Long = if (bool) 1 else 0
  def intToBool(integer: Long): Boolean = if (integer == 1) true else false

  /** Given a sequence of bits, interpret it as an unsigned binary number and return the number.
    *
    * Scala hint: Consult the Scala library documentation for classes such as Seq:
    * https://www.scala-lang.org/api/current/scala/collection/immutable/Seq.html
    *
    * The purpose of this assignment is for *you* to write code that encodes/decodes numbers in binary.
    * Do not submit solutions that just call functions in the Java/Scala standard library to do the
    * conversion for you.
    **/

  def decodeUnsigned(bits: Seq[Boolean]): Long = {
    if (bits.isEmpty) 0
    else boolToInt(bits.last) + 2 * decodeUnsigned(bits.dropRight(1))
  }

  /** Given a sequence of bits, interpret it as a signed two's-complement binary number and return the number. */
  def decodeSigned(bits: Seq[Boolean]): Long = {
    require(bits.nonEmpty)
    if (bits.length == 1) -boolToInt(bits.last)
    else boolToInt(bits.last) + 2 * decodeSigned(bits.dropRight(1))
  }

  /** Given a non-negative number `i`, encode it as an unsigned binary number using the number of bits
    * specified by `bits`.
    *
    * Scala hint: The `bits: Int = 32` specifies `32` as the default value of bits. When calling this method, one
    * can specify the number of bits explicitly (e.g. `encodeUnsigned(42, 8)`), or leave it unspecified
    * (e.g. `encodeUnsigned(42)`), in which case the default value of 32 will be used.
    *
    * The length of the output sequence must be equal to `bits`.
    **/
  @tailrec
  def doEncodeUnsigned(i: Long, bits: Int = 32, unsignedSeq: Seq[Boolean]): Seq[Boolean] = {
    if (bits == 0) unsignedSeq
    else doEncodeUnsigned(i / 2, bits - 1, intToBool(i%2)+:unsignedSeq)
  }

  def encodeUnsigned(i: Long, bits: Int = 32): Seq[Boolean] = {
    require(i >= 0)
    require(i < twoTo(bits))
    doEncodeUnsigned(i, bits, Seq())
  }

  /** Given a number `i`, encode it as a signed two's-complement binary number using the number of bits
    * specified by `bits`.
    *
    * The length of the output sequence must be equal to `bits`.
    **/

  def encodeSigned(i: Long, bits: Int = 32): Seq[Boolean] = {
    require(i >= -twoTo(bits-1))
    require(i < twoTo(bits-1))
    if (i < 0) doEncodeUnsigned(Math.abs(i + 1), bits, Seq()).map((bool: Boolean) => if (bool) false else true) else doEncodeUnsigned(i, bits, Seq())
  }


  /* Before continuing Assignment 1, go to `A1.scala` and complete the methods there. Then return here and implement
   * the following.
   */

  

  /* Each of the following methods should encode the corresponding MIPS machine language instruction as a 32-bit `Word`.
   *
   * Hint: One way to create a word is from a sequence of 32 Booleans.
   * One way to create a sequence of Booleans is using Bits.
   *
   * For example:
   * `val fourBits = Seq(true, false, true, false)`
   * `val moreBits = Bits("0101")`
   * `val eightBits = fourBits ++ moreBits`
   * `val word = Word(eightBits ++ eightBits ++ eightBits ++ eightBits)`
   */

  /* Hint: You may implement additional helper methods if you wish to factor out common code. */

  def WORD(number: Long): Word = Word(encodeSigned(number))

  // Add & Sub
  def ADD(d: Reg, s: Reg, t: Reg = Reg.zero): Word = Word(Bits("000000") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ encodeUnsigned(d.number, 5) ++ Bits("00000100000"))
  def SUB(d: Reg, s: Reg, t: Reg): Word = Word(Bits("000000") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ encodeUnsigned(d.number, 5) ++ Bits("00000100010"))

  // Multiply (Un)signed, Division (Un)signed, Move From HI/LO, Load Immediate and Skip
  def MULT(s: Reg, t: Reg): Word =  Word(Bits("000000") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ Bits("0000000000011000"))
  def MULTU(s: Reg, t: Reg): Word = Word(Bits("000000") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ Bits("0000000000011001"))
  def DIV(s: Reg, t: Reg): Word = Word(Bits("000000") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ Bits("0000000000011010"))
  def DIVU(s: Reg, t: Reg): Word = Word(Bits("000000") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ Bits("0000000000011011"))
  def MFHI(d: Reg): Word = Word(Bits("0000000000000000") ++ encodeUnsigned(d.number, 5) ++ Bits("00000010000"))
  def MFLO(d: Reg): Word = Word(Bits("0000000000000000") ++ encodeUnsigned(d.number, 5) ++ Bits("00000010010"))
  def LIS(d: Reg): Word = Word(Bits("0000000000000000") ++ encodeUnsigned(d.number, 5) ++ Bits("00000010100"))

  // Load/Store word
  def LW(t: Reg, i: Int, s: Reg): Word = Word(Bits("100011") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ encodeSigned(i, 16))
  def SW(t: Reg, i: Int, s: Reg): Word = Word(Bits("101011") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ encodeSigned(i, 16))

  // Set less than (Un)signed
  def SLT(d: Reg, s: Reg, t: Reg): Word =  Word(Bits("000000") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ encodeUnsigned(d.number, 5) ++ Bits("00000101010"))
  def SLTU(d: Reg, s: Reg, t: Reg): Word = Word(Bits("000000") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ encodeUnsigned(d.number, 5) ++ Bits("00000101011"))

  // Branch on (NOT) Equal
  def BEQ(s: Reg, t: Reg, i: Int): Word = Word(Bits("000100") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ encodeSigned(i, 16))
  def BNE(s: Reg, t: Reg, i: Int): Word = Word(Bits("000101") ++ encodeUnsigned(s.number, 5) ++ encodeUnsigned(t.number, 5) ++ encodeSigned(i, 16))
  def JR(s: Reg): Word = Word(Bits("000000") ++ encodeUnsigned(s.number, 5) ++ Bits("000000000000000001000"))
  def JALR(s: Reg): Word = Word(Bits("000000") ++ encodeUnsigned(s.number, 5) ++ Bits("000000000000000001001"))
}
