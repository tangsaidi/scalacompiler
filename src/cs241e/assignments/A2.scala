package cs241e.assignments

import Assembler._
import ProgramRepresentation._
import Transformations._
import cs241e.mips._

object A2 {
  /* As part of Assignment 2, before implementing the methods in this file, first implement the methods in
   * Transformations.scala in the section for Assignment 2.
   */

  /* Registers 1 and 2 hold 32-bit integers (in two's-complement notation). Place the maximum of these integers
   * in register 3, and return.
   */

  lazy val maximum: Seq[Word] = {
    val maxLabel = new Label("maxLabel")
    val code = Seq[Code](CodeWord(SLT(Reg(3), Reg(1), Reg(2))),
      beq(Reg(3), Reg(0), maxLabel),
      CodeWord(ADD(Reg(3), Reg(2))),
      CodeWord(JR(Reg(31))),
      Define(maxLabel),
      CodeWord(ADD(Reg(3), Reg(1))),
      CodeWord(JR(Reg(31)))
    )
    eliminateLabels(code)
  }

  /* Registers 1 and 2 hold 32-bit integers (in unsigned integer notation). Place the maximum of these integers
   * in register 3, and return.
   */
  lazy val maximumUnsigned: Seq[Word] = {
    val maxUnsignedLabel = new Label("maxUnsignedLabel")
    val code = Seq[Code](CodeWord(SLTU(Reg(3), Reg(1), Reg(2))),
      beq(Reg(3), Reg(0), maxUnsignedLabel),
      CodeWord(ADD(Reg(3), Reg(2))),
      CodeWord(JR(Reg(31))),
      Define(maxUnsignedLabel),
      CodeWord(ADD(Reg(3), Reg(1))),
      CodeWord(JR(Reg(31)))
    )
    eliminateLabels(code)
  }


//  lazy val A2Test: Seq[Word] = {
//    val maxUnsignedLabel = new Label("maxUnsignedLabel")
//    val code = Seq[Code](CodeWord(SLTU(Reg(3), Reg(1), Reg(2))),
//      Block(Seq(beq(Reg(3), Reg(0), maxUnsignedLabel),
//        Block(Seq(Comment("hahahaha"))),
//        CodeWord(ADD(Reg(3), Reg(2))))),
//      CodeWord(JR(Reg(31))),
//      Comment("hahahaha"),
//      Comment("hahahaha"),
//      Define(maxUnsignedLabel),
//      CodeWord(ADD(Reg(3), Reg(1))),
//      CodeWord(JR(Reg(31)))
//    )
//    eliminateLabels(eliminateComments(code.flatMap(eliminateBlocks)))
//  }



}

