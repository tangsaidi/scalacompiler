package cs241e.assignments

import ProgramRepresentation._
import CodeBuilders._
import cs241e.assignments.Assembler.{LIS, encodeSigned}
import cs241e.mips.Word
import Assembler._

object A5 {
  /** The code of `printInteger` from Assignment 4 encapsulated as a `Procedure`. The procedure should have
    * exactly one parameter, the integer to be printed. */

  lazy val printProcedure: Procedure = {

    val k: Variable = new Variable("k")
    val procedure = new Procedure("printProcedure", Seq(k))
    procedure.code = block(
      ADD(Reg(16), Reg.input1),
      ADD(Reg.input1, Reg.result),
      A4.printIntegerCode,
      ADD(Reg.input1, Reg(16))
    )
    procedure
  }

  /** This procedure will be executed with an array of 32-bit integers as in Assignment 4.
    * It should take two parameters: the first is the address of the beginning of the array
    * and the second is the number of elements in the array.
    * The procedure should call `printProcedure` for each integer in the array in turn,
    * to print all the integers, and return.
    *
    * Test this procedure by compiling it with `printProcedure` and running it on various arrays.
    */

  def writeToResult(num: Int) = block(
    LIS(Reg.result),
    Word(encodeSigned(num))
  )

  def readFromVar(v: Variable) = read(Reg.result, v)

  lazy val printArray: Procedure = {
    val i: Variable = new Variable("i")
    val j: Variable = new Variable("j")
    val procedure = new Procedure("printArray", Seq(i, j))


    procedure.code = ifStmt(block(readFromVar(j)), gtCmp, ADD(Reg.result, Reg.zero),
      block(call(printProcedure, block(
        binOp(binOp(ADD(Reg.result, Reg.input2), minus, readFromVar(j)), times, writeToResult(4)), ADD(Reg.result, Reg.input1, Reg.result), LW(Reg.result, 0, Reg.result))), // Find out which number we are at based on the incrementor
        ADD(Reg.result, Reg.input2), ADD(Reg(17),Reg.result),
        call(procedure, ADD(Reg.result, Reg.input1), binOp(readFromVar(j), minus, writeToResult(1)))))

    procedure
  }

  /** This procedure will be executed with an array of 32-bit integers as in Assignment 4.
    * It should take two parameters: the first is the address of the beginning of the array
    * and the second is the number of elements in the array.
    *
    * You may use multiple procedures if you wish. Generate them and return them in a `Seq`.
    * The tests will execute the first procedure in the sequence.
    *
    * The task is to determine the height of a binary tree and return it (in `Reg.result`).
    * Assume that every tree contains at least one node and hence has a height of at least one.
    * Each node of the tree is encoded in three consecutive elements (words) of the array:
    * a two's-complement integer stored at the node, the node's left child, and the node's right child.
    * Each child is specified as the array index of the first element of the child node.
    * The integer -1 indicates that a node does not have a left or right child. For example, the following tree:
    *
    *   77
    *  /  \
    * 22    -8
    *     /  \
    *   -36   999
    *
    * could be encoded by following array:
    *
    * A[0] = 77
    * A[1] = 3
    * A[2] = 6
    * A[3] = 22
    * A[4] = -1
    * A[5] = -1
    * A[6] = -8
    * A[7] = 9
    * A[8] = 12
    * A[9] = -36
    * A[10] = -1
    * A[11] = -1
    * A[12] = 999
    * A[13] = -1
    * A[14] = -1
    *
    * in which the root is encoded by the elements A[0], A[1] and A[2], the root's left child is encoded
    * by the elements A[3], A[4] and A[5], the root's right child is encoded by the elements A[6], A[7] and A[8],
    * the root's left-most grandchild is encoded by the elements A[9], A[10] and A[11],
    * and the root's right-most grandchild is encoded by the elements A[12], A[13] and A[14].
    *
    * This example tree has height 3.
    */
  lazy val treeHeight: Seq[Procedure] = {

    val absoluteBeginning: Variable = new Variable("Absolute beginning")
    val beginningAddr: Variable = new Variable("Beginning of the array")

    lazy val getTreeDepth: Procedure = new Procedure("GetTreeDepth", Seq(beginningAddr, absoluteBeginning))

    // Reg(19) will only be used to hold the max tree depth
    // Reg(20) will be used as a temp register to hold the depth of current tree
    getTreeDepth.code =
      block(
        ifStmt(block(writeToResult(1), ADD(Reg(20), Reg.result, Reg(20)), ADD(Reg.result, Reg(20))), geCmp, ADD(Reg.result, Reg(19)),
          ADD(Reg(19), Reg(20))),

        Comment("LOADING BEGINNING ADDR"),
        readFromVar(beginningAddr),
        ifStmt(block(readFromVar(beginningAddr), LW(Reg.result, 4, Reg.result)), neCmp, block(LIS(Reg.result), WORD(-1)),
          block(Comment("checking left branch"), call(getTreeDepth, binOp(binOp(block(readFromVar(beginningAddr), LW(Reg.result, 4, Reg.result)),
            times, writeToResult(4)),
            plus, readFromVar(absoluteBeginning)), readFromVar(absoluteBeginning)))
        ),

        ifStmt(block(readFromVar(beginningAddr), LW(Reg.result, 8, Reg.result)), neCmp, block(LIS(Reg.result), WORD(-1)),
          block(Comment("checking right branch"), call(getTreeDepth, block(binOp(binOp(block(readFromVar(beginningAddr), LW(Reg.result, 8, Reg.result)),
            times, writeToResult(4)),
            plus, readFromVar(absoluteBeginning))), readFromVar(absoluteBeginning)))
        ),
        writeToResult(1),
        SUB(Reg(20), Reg(20), Reg.result),
        ADD(Reg.result, Reg(19))
    )

    lazy val treeStartProc: Procedure = new Procedure("Starter for get tree depth", Seq(beginningAddr, absoluteBeginning))

    treeStartProc.code = block(
      Comment("calling getTreeDepth"),
      call(getTreeDepth, readFromVar(beginningAddr), readFromVar(beginningAddr))
    )
    Seq(treeStartProc, getTreeDepth)
  }
}
