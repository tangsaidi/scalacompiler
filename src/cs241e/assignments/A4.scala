package cs241e.assignments

import cs241e.mips._
import Assembler._
import Transformations._
import ProgramRepresentation._
import CodeBuilders._
import A1._

object A4 {
  /** This is an enhanced version of the loadAndRun method from Assignment 1.
    *
    * It loads the give machine language code into memory, writes the specified values to registers 1 and 2,
    * and then runs the CPU on the code that was loaded. The debugger can be invoked by passing the argument
    * debug = true.
    */
  def loadAndRun(code: MachineCode, register1: Word = Word.zero, register2: Word = Word.zero, debug: Boolean = false): State = {
    val initialState =
      setMem(code.words)
        .setReg(1, register1)
        .setReg(2, register2)
    if(debug) Debugger.debug(initialState, code.debugTable)
    else CPU.run(initialState)
  }

  /** A utility method that takes two sequences of words representing `code` and an `array`,
    * loads both into memory, and sets register 1 to the address of the beginning
    * of the array and register 2 to its length in words.
    */
  def loadCodeAndArray(code: Seq[Word], array: Seq[Word]): State = {
    val arrayAddress: Word = Word(encodeUnsigned(code.size * 4))
    val arrayLength: Word = Word(encodeUnsigned(array.size))
    val loadedCode: State = setMem(code)
    val loadedArray: State = setMem(array, loadedCode, arrayAddress)
    loadedArray.setReg(1, arrayAddress).setReg(2, arrayLength)
  }

  /** A utility method that loads code and an array into memory and runs the code.
    * The debugger can be invoked by passing the argument debug = true.
    */
  def loadAndRunArray(code: MachineCode, array: Seq[Word], debug: Boolean = false): State = {
    val state = loadCodeAndArray(code.words, array)
    if(debug) Debugger.debug(state, code.debugTable)
    else CPU.run(state)
  }

  /** Register 1 holds the address of the beginning of an array of 32-bit integers.
    * Register 2 holds the number of elements in the array.
    * If the array is empty place the value -1 in register 3.
    * Otherwise copy the last element of the array into register 3.
    */
  lazy val lastElement: MachineCode = {
    val code: Code = ifStmt(ADD(Reg.result, Reg(2)),eqCmp, ADD(Reg.result,Reg.zero),
      block(LIS(Reg.result), WORD(-1), JR(Reg(31))),
      block(LIS(Reg.scratch), WORD(4), ADD(Reg.result, Reg(2)), times,
        SUB(Reg.result, Reg.result, Reg.scratch), ADD(Reg.result, Reg.result, Reg(1)),LW(Reg.result, 0, Reg.result)))
    compilerA4(code)
  }

  /** Register 1 holds the address of the beginning of an array of 32-bit two's-complement integers.
    * Register 2 holds the number of elements in the array.
    * Determine the maximum of all the elements of the array, write it into register 3.
    * Assume the array is not empty.
    */
  lazy val arrayMaximum: MachineCode = {
    val incVar: Variable = new Variable("Increment Counter")
    // Reg(10) will be used as a counter, Reg(11) holds constant 1, Reg(12) holds temporary max, Reg(13) used as an extra scratch
    val code: Code = block(LIS(Reg(11)), WORD(1), ADD(Reg(10), Reg.zero), LW(Reg(12), 0, Reg(1)), // Initialize the registers
      whileLoop(ADD(Reg.result, Reg(10)), ltCmp, ADD(Reg.result, Reg(2)), // Compare the value in Reg(10) against Reg(2)
      block(ADD(Reg.scratch, Reg(10)), LIS(Reg.result), WORD(4), times, ADD(Reg.result, Reg.result, Reg(1)), // Calculate the address of next element
      LW(Reg.result, 0, Reg.result), ifStmt(ADD(Reg(13), Reg.result), gtCmp, ADD(Reg.result, Reg(12)), ADD(Reg(12), Reg(13))), // Compare max and store
      ADD(Reg(10), Reg(10), Reg(11)))), //increment counter by 1
      ADD(Reg.result, Reg(12)))

    compilerA4(code)
  }

  /** Register 1 holds the address of the beginning of an array of 32-bit integers, each representing a character.
    * The integer zero represents a space, and each integer i (1 <= i <= 26) represents the i'th letter of the
    * uppercase alphabet.
    * Register 2 holds the number of elements in the array (can be empty).
    * Your program should output the uppercase characters represented by the integers in the array.
    * The MIPS system allows you to output one character at a time, by storing its ASCII value into the
    * special memory location ffff000c (hex).
    *
    * Hint: use Google to find an ASCII code table.
    */
  
  lazy val outputLetters: MachineCode = {
    // Reg(10) will be used as a counter, Reg(11) holds constant 1, Reg(12) holds ASCII Offset, Reg(13) used as an extra scratch, Reg(14) stores the adddress ffff000c
    val code: Code = block(LIS(Reg(11)), WORD(1), ADD(Reg(10), Reg.zero), LW(Reg(12), 0, Reg(1)), LIS(Reg(12)), WORD(64), LIS(Reg(14)), Word(encodeUnsigned(4294901772L)),// Initialize the registers
      whileLoop(ADD(Reg.result, Reg(10)), ltCmp, ADD(Reg.result, Reg(2)), // Compare the value in Reg(10) against Reg(2), for while loop
        block(ADD(Reg.scratch, Reg(10)), LIS(Reg.result), WORD(4), times, ADD(Reg.result, Reg.result, Reg(1)), // Calculate the address of next element
          LW(Reg.result, 0, Reg.result), ifStmt(ADD(Reg(13), Reg.result), eqCmp, ADD(Reg.result, Reg.zero), //check whether we need a number or space
            block(LIS(Reg(13)), WORD(32), SW(Reg(13), 0, Reg(14))), // Store a space into ffff000c
            block(ADD(Reg(13), Reg(13), Reg(12)), SW(Reg(13), 0, Reg(14)))), // Store the value in Register 13 into ffff000c
          ADD(Reg(10), Reg(10), Reg(11)))))//increment counter by 1
    compilerA4(code)
  }

  /** Register 1 holds a 32-bit integer (in two's-complement notation).
    * Your program should format this integer in base 10, print it, then print a newline character.
    */
  // We keep Reg(1) as it is, copy the value to Reg(2) and take remainder on Reg(2)
  // Reg(10) holds a constant 10
  // Reg(11) is a zero flag, it is 1 when the zeros are leading zeros
  // Reg(12) holds a constant offset 48 for ASCII Conversion
  // Reg(13) holds the -2,147,483,648, which does not have a corresponding positive integer
  // Reg(14) stores the output memory address
  // Reg(15) is a counter for number of digits in decimal
  lazy val printIntegerCode: Code = {
    val varSeq = Seq.tabulate(10)(va => new Variable(va.toString))
    val endLabel = new Label("END LABEL")

    Scope(varSeq, block(ADD(Reg(18), Reg(1)), LIS(Reg(10)), WORD(10), LIS(Reg(14)), Word(encodeUnsigned(4294901772L)), LIS(Reg(11)), WORD(1),
      LIS(Reg(11)), WORD(1), LIS(Reg(12)), WORD(48), ADD(Reg(15), Reg.zero), LIS(Reg(13)), Word(encodeSigned(-2147483648)),

      ifStmt(ADD(Reg.result, Reg(1)), ltCmp, ADD(Reg.result, Reg.zero), // check whether the number is negative
        block(LIS(Reg.result), WORD(45), SW(Reg.result, 0, Reg(14)))), // print the negative sign for negative numbers first


      ifStmt(ADD(Reg.result, Reg(1)), eqCmp, ADD(Reg.result, Reg(13)), block(
        LIS(Reg.result), WORD(50), SW(Reg.result, 0, Reg(14)), LIS(Reg.result), WORD(49), SW(Reg.result, 0, Reg(14)),
        LIS(Reg.result), WORD(52), SW(Reg.result, 0, Reg(14)), LIS(Reg.result), WORD(55), SW(Reg.result, 0, Reg(14)),
        LIS(Reg.result), WORD(52), SW(Reg.result, 0, Reg(14)), LIS(Reg.result), WORD(56), SW(Reg.result, 0, Reg(14)),
        LIS(Reg.result), WORD(51), SW(Reg.result, 0, Reg(14)), LIS(Reg.result), WORD(54), SW(Reg.result, 0, Reg(14)),
        LIS(Reg.result), WORD(52), SW(Reg.result, 0, Reg(14)), LIS(Reg.result), WORD(56), SW(Reg.result, 0, Reg(14)), // This line is not fun, basically hardcoded printing 2147483648
        LIS(Reg.result), Use(endLabel), JR(Reg.result))),

      ifStmt(ADD(Reg.result, Reg(1)), eqCmp, ADD(Reg.result, Reg.zero), block(SW(Reg(12), 0, Reg(14)), LIS(Reg.result), Use(endLabel), JR(Reg.result))),

      ifStmt(ADD(Reg.result, Reg(1)), ltCmp, ADD(Reg.result, Reg.zero), // check whether the number is negative
        block(SUB(Reg(18), Reg.zero, Reg(1)))), // print the negative sign for negative numbers first

      Block(varSeq.map{va: Variable => block(
        DIV(Reg(18), Reg(10)),
        MFLO(Reg(18)),
        MFHI(Reg.scratch),
        write(va, Reg.scratch)
      )}),

      Block(varSeq.reverseMap{va: Variable => block(
        ifStmt(read(Reg.result, va), neCmp, ADD(Reg.result, Reg.zero),
          block(read(Reg.result, va), ADD(Reg.result, Reg.result, Reg(12)),SW(Reg.result, 0, Reg(14)), ADD(Reg(11), Reg.zero)),
          ifStmt(ADD(Reg.result, Reg(11)), eqCmp, ADD(Reg.result, Reg.zero), SW(Reg(12), 0, Reg(14)))
      ))}),
      Define(endLabel),
      SW(Reg(10),0, Reg(14)) // print '\n', the ascii code happens to be 10
  ))
  }

  lazy val printInteger: MachineCode = compilerA4(printIntegerCode)
}
