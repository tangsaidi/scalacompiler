package cs241e.assignments

import cs241e._
import ProgramRepresentation._
import CodeBuilders._
import Typer._
import cs241e.scanparse.DFAs.Token
import scanparse.Grammars._
import Assembler._

/** A code generator that converts a Lacs parse tree into the intermediate language developed in Assignment 1 to 6. */
object CodeGenerator {
  def generateProcedures(procedureScopes: Seq[ProcedureScope], typeMap: Map[Tree, Type]): Seq[Procedure] = {

    /** Given a `procedureScope`, generates the `Code` implementing the procedure, and assigns it to
      * `procedureScope.procedure.code`.
      */
    def generateCode(procedureScope: ProcedureScope): Unit = {

      def idToVariable(idTree: Tree): Variable = {
        procedureScope.symbolTable(idTree.lhs.lexeme) match {
          case Left(value) => value.variable
        }
      }
      def idToCode(idTree: Tree): Code = {
        procedureScope.symbolTable(idTree.lhs.lexeme) match {
          case Left(value) => read(Reg.result, value.variable)
          case Right(value) => Closure(value.procedure)
        }
      }
      def operatorToCode(operatorTree: Tree): Code = {
        operatorTree.lhs.kind match {
          case "PLUS" => plus
          case "MINUS" => minus
          case "STAR" => times
          case "SLASH" => divide
          case "PCT" => remainder
        }
      }
      def comparatorToCode(comparatorTree: Tree): Label => Code = {
        comparatorTree.lhs.kind match {
          case "GT" => gtCmp
          case "GE" => geCmp
          case "LT" => ltCmp
          case "LE" => leCmp
          case "EQ" => eqCmp
          case "NE" => neCmp
        }
      }
      /** Generates the `Code` that implements `tree`.
        *
        * This method will be called from the outside only on `tree`s rooted at nodes of kind "expras".
        * However, if it calls itself recursively on other kinds of nodes, it needs to be able to handle
        * those node kinds as well.
        */
      def recur(tree: Tree): Code = {
        val children = tree.children
        val treeKind = tree.lhs.kind
        treeKind match {
          case "NUM" =>  block(LIS(Reg.result), WORD(tree.lhs.lexeme.toLong))
          case "ID" => idToCode(tree)
          case "expras" => Block(collect(tree, "expra").map{ x => recur(x)})
          case "expra" => if (children.length == 3) assign(idToVariable(children.head), recur(children.last))
          else recur(children.head)
          case "expr" => if (children.length == 3) binOp(recur(children.head), operatorToCode(children(1)), recur(children.last))
            else if (children.length == 1) recur(children.head)
            else ifStmt(recur(children(2).children.head),comparatorToCode(children(2).children(1)) , recur(children(2).children.last),
              recur(children(5)), recur(children(9)))
          case "term" =>
            if (children.length == 1) recur(children.head)
            else binOp(recur(children.head), operatorToCode(children(1)), recur(children.last))
          case "factor" =>
            children.head.lhs.kind match {
              case "ID" => idToCode(children.head)
              case "NUM" => block(LIS(Reg.result), WORD(children.head.lhs.lexeme.toLong))
              case "LPAREN" => recur(children(1))
              case "factor" => {
                val nextLevel = children.head.children.head
                typeMap(children.head) match {
                  case FunctionType(parameterTypes, returnType) => {
                    nextLevel.lhs.kind match {
                      case "ID" => procedureScope.symbolTable(nextLevel.lhs.lexeme) match {
                        case Left(idType) => CallClosure(recur(nextLevel), collect(children(2), "expr").map(recur),
                          parameterTypes.map(x => makeVariable("temp Var", x)))
                        case Right(idType) => Call(idType.procedure, collect(children(2), "expr").map(recur))
                      }
                      case _ => CallClosure(recur(children.head), collect(children(2), "expr").map(recur),
                        parameterTypes.map(x => makeVariable("temp Var", x)))
                    }
                  }
                }
              }
              }
          case _ => throw new RuntimeException(s"Unrecognized tree type $treeKind")
        }
      }

      /* Main body of generateCode. */
      procedureScope.procedure.code = Scope(procedureScope.variables.map(_.variable), recur(procedureScope.expras))
    }

    /* Main body of generateProcedures. */

    procedureScopes.foreach(generateCode)
    procedureScopes.map(_.procedure)
  }
}
