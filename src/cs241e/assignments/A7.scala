package cs241e.assignments

import cs241e.scanparse.DFAs._

object A7 {
  /** A sample DFA with alphabet {0,1} that recognizes binary integers that have no useless (leading) zeroes. */
  val binaryNumbers = DFA(
    alphabet = "01".toSet,
    states = Set("start", "0", "not0"),
    start = "start",
    accepting = Set("0", "not0"),
    transition = {
      case ("start", '0') => "0"
      case ("start", '1') => "not0"
      case ("not0", _) => "not0"
    })

  /** A DFA with alphabet {0,1} that recognizes binary integers that have no useless (leading) zeroes
    * and are not divisible by 3.
    */
  lazy val notDiv3 = DFA(
    alphabet = "01".toSet,
    states = Set("start", "0", "remainder1","remainder2", "divisible"),
    start = "start",
    accepting = Set("remainder1", "remainder2"),
    transition = {
      case ("start", '0') => "0"
      case ("start", '1') => "remainder1"
      case ("remainder1" , '0') => "remainder2"
      case ("remainder2", '0') => "remainder1"
      case ("remainder2", '1') => "remainder2"
      case ("divisible", '0') => "divisible"
      case ("divisible", '1') => "remainder1"
      case ("remainder1", '1') => "divisible"
    })

  /** A DFA with alphabet {0,1} that recognizes binary integers that have no useless (leading) zeroes
    * and are not divisible by 2 or by 3.
    */
  lazy val notDiv23 = DFA(
    alphabet = "01".toSet,
    states = Set("start", "0", "remainder1", "remainder2", "remainder1even", "remainder2even", "divisible"),
    start = "start",
    accepting = Set("remainder1", "remainder2"),
    transition = {
      case ("start", '0') => "0"
      case ("start", '1') => "remainder1"
      case ("remainder1", '0') => "remainder2even"
      case ("remainder1", '1') => "divisible"
      case ("remainder2even", '0') => "remainder1even"
      case ("remainder2even", '1') => "remainder2"
      case ("remainder2", '0') => "remainder1even"
      case ("remainder2", '1') => "remainder2"
      case ("remainder1even", '0') => "remainder2even"
      case ("remainder1even", '1') => "divisible"
      case ("divisible", '0') => "divisible"
      case ("divisible", '1') => "remainder1"
    })

  /** A DFA that recognizes a decimal number between -128 and 127 inclusive, with no useless zeroes.
    * (Zeroes are required and only permitted if removing them changes the meaning of the number.)
    * The alphabet symbols are {0,1,2,3,4,5,6,7,8,9,-}.
    */
  lazy val decimalNumber = DFA(
    alphabet = "-0123456789".toSet,
    states = Set("start", "negative", "0", "+1", "+1/0-1","+1/0-1/0-9", "+1/2","+1/2/0-7", "+1/3-9", "+2-9", "+2-9/0-9",
      "-1", "-1/2","-1/2/0-8"),
    start = "start",
    accepting = Set("0", "+1", "+1/0-1","+1/0-1/0-9", "+1/2","+1/2/0-7", "+1/3-9", "+2-9", "+2-9/0-9",
      "-1","-1/2","-1/2/0-8"),
    transition = {
      case ("start", '0') => "0"
      case ("start", '-') => "negative"
      case ("start", x) if '2' <= x && '9' >= x => "+2-9"
      case ("start", x) if x == '1' => "+1"
      case ("negative", x) if '2' <= x && '9' >= x => "+2-9"
      case ("negative", x) if '1' == x => "-1"
      case ("+2-9", x) if '0' <= x && '9' >= x => "+2-9/0-9"
      case ("+1", x) if '3' <= x && '9' >= x => "+1/3-9"
      case ("+1", x) if '2' == x => "+1/2"
      case ("+1/2", x) if  '0' <= x && '7' >= x => "+1/2/0-7"
      case ("+1", x) if '0' <= x && '1' >= x => "+1/0-1"
      case ("+1/0-1", x) if '0' <= x && '9' >= x => "+1/0-1/0-9"
      case ("-1", x) if '3' <= x && '9' >= x => "+1/3-9"
      case ("-1", x) if '0' <= x && '1' >= x => "+1/0-1"
      case ("-1", x) if '2' == x => "-1/2"
      case ("-1/2", x) if '0' <= x && '8' >= x => "-1/2/0-8"
    }
  )

  /** A DFA with alphabet {a, b, c} that recognizes any string that contains all three letters in
    * alphabetical order (i.e. "abc"), possibly interspersed with more letters. For example, "acbac"
    * and "cbacbacba" are in the language, but not "acba".
    */
  lazy val abc = DFA(
    alphabet = "abc".toSet,
    states = Set("start", "a", "b", "c"),
    start = "start",
    accepting = Set("c"),
    transition = {
      case ("start", 'a') => "a"
      case ("start", 'b') => "start"
      case ("start", 'c') => "start"
      case ("a", 'a') => "a"
      case ("a", 'b') => "b"
      case ("a", 'c') => "a"
      case ("b", 'a') => "b"
      case ("b", 'b') => "b"
      case ("b", 'c') => "c"
      case ("c", 'a') => "c"
      case ("c", 'b') => "c"
      case ("c", 'c') => "c"
    }
  )

  /** A DFA that recognizes any string from the alphabet {a,b,c} containing abc as a substring. */
  lazy val abcSubstring = DFA(
    alphabet = "abc".toSet,
    states = Set("start", "a", "b", "c"),
    start = "start",
    accepting = Set("c"),
    transition = {
      case ("start", 'a') => "a"
      case ("start", 'b') => "start"
      case ("start", 'c') => "start"
      case ("a", 'a') => "a"
      case ("a", 'b') => "b"
      case ("a", 'c') => "start"
      case ("b", 'a') => "a"
      case ("b", 'b') => "start"
      case ("b", 'c') => "c"
      case ("c", 'a') => "c"
      case ("c", 'b') => "c"
      case ("c", 'c') => "c"
    }
  )
}
